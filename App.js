import React from "react";
import "react-native-gesture-handler";

import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import thunkMiddleware from "redux-thunk";
import reducers from "./src/redux/reducer/index";

import { persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/es/integration/react";
import MainNavigation from "./src/utilities/MainNav";
import { LogBox,StatusBar } from 'react-native';

LogBox.ignoreLogs([
  "[react-native-gesture-handler] Seems like you\'re using an old API with gesture components, check out new Gestures system!",
]);

const middleware = [thunkMiddleware];
const store = compose(applyMiddleware(...middleware))(createStore)(reducers);

let persistor = persistStore(store);


export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    // if (!this.state.isAppInitiated) {
    //   return null;
    // }

    return (
        <Provider store={store}>
          <StatusBar backgroundColor="#000"/>
          <PersistGate persistor={persistor}>
              <MainNavigation />
          </PersistGate>
        </Provider>
    );
  }
}  