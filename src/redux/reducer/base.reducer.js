import * as ActionTypes from "../action/ActionsType";

const initialState = {
  movies: null,
  isMoviesFetching: false,
  moviesMessage: null,
  moviesActionType: null,
};

export default function base(state = initialState, action) {
  switch (action.type) {
      case ActionTypes.GET_MOVIES_PENDING: {
          console.log("GET_MOVIES_PENDING ++++  reducer")

          return {
              ...state,
              moviesActionType: action.type,
              isMoviesFetching: true,
          };
      }

      case ActionTypes.GET_MOVIES_SUCCESS: {
          console.log("GET_MOVIES_SUCCESS ++++  reducer", action, state)
          return {
              ...state,
              moviesActionType: action.type,
              moviesMessage: action.message,
              movies: action.payload['body'],
              isMoviesFetching: false,
              // isWishlistErr: false,
              moviesStatusCode: action.payload.statusCode
          };
      }

      case ActionTypes.GET_MOVIES_FAIL: {
          console.log("GET_MOVIES_FAIL ++++  reducer", action, state)
          return {
              ...state,
              moviesActionType: action.type,
              moviesMessage: action.message,
              isMoviesFetching: false,
              moviesStatusCode: action.message.statusCode
          };
      }

      default:
          return state;
  }

}