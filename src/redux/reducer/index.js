import { persistCombineReducers } from "redux-persist";
import AsyncStorage from '@react-native-async-storage/async-storage';

import movieReducer from "./base.reducer";

const config = {
  key: "root",
  storage: AsyncStorage,
  keyPrefix: '',
  timeout: null,
  whitelist: [
    "movieReducer",

  ]
};
const reducers = persistCombineReducers(config, {
  movieReducer,
 


});


const rootReducer = (state, action) => {
  // when a logout action is dispatched it will reset redux state
  // console.log("IUHINHDLLHJKD", state)


  return reducers(state, action);
};


export default rootReducer;
