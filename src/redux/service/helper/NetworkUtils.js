import axios from "axios";


class NetworkUtils {
  constructor(options) {
    this.baseUrl = options.baseUrl;
  }



  get(endpoint, token = null) {
    console.log("this.baseUrl",this.baseUrl)
    return this.requestHttp("GET", this.baseUrl, null);
  }

  post(endpoint, params, token = null) {
    console.log('inside post method');
    
    return this.requestHttp("POST", this.baseUrl + endpoint, params);
  }

  put(endpoint, params, token = null) {
    return this.requestHttp("PUT", this.baseUrl + endpoint, params);
  }

  delete(endpoint, params, token = null) {
    return this.requestHttp("DELETE", this.baseUrl + endpoint, params);
  }

 


  requestHttp(method, url, params) {
    

    return new Promise((resolve, reject) => {
          var formData = new  URLSearchParams();// FormData();
          // console.log("deviceType  ", deviceType)

       
          
         console.log("Api form Data", formData)
      //   if(params)
      //   {
      //     for (var key in params) {
      //       formData.append(key,params[key]);
      //     }
      //   }

      //   var object = {};
      //   if(params)
      //   {
      //     for (var key in params) {
      //       // formData.append(key,params[key]);
      //       object[key] = params[key];
      //     }
      //   }

       
      //   var json = JSON.stringify(object);

      //   console.log("JSONNNN",json)
        

      // var options = {
      //   method,
      //   url,
      //   headers: {
      //     'Content-Type':'application/json'
      //     // 'Content-Type':'multipart/form-data'
      //   },
      //   data:json
      // };
      
      if(params)
      {
        for (var key in params) {
          formData.append(key,params[key]);
        }
      }

      console.log("URL+++<<<>>>>>>><<>><<>><<>>>>",url)
    var options = {
      method,
      url,
      headers: {
        'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8'
        // 'Content-Type':'multipart/form-data'
      },
      data:formData.toString()
    };
      axios(options)
        .then(response => {
          resolve({ statusCode: response.status, body: response.data });
        })
        .catch(error => {
          console.log("API Errr ++++++++++++++++++++++",error)
          if (error.response != undefined) {
            resolve({
              statusCode: error.response.status,
              body: error.response.data
            });
          } else {
            reject(("Can not connect to server"));
          }
        });
    });
  }
}

export default NetworkUtils;
