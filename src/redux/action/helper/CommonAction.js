export const CommonAction = (url, params, actions, Service) => {
  return (dispatch) => {
    dispatch({ type: actions.pending });
    Service(url, params)
      .then((res) => {
        console.log(url + " <<<<<< Action >>>>>>>>> respo", res);
        dispatch({
          type: actions.success,
          payload: res,
          message: res.message,
        });
      })
      .catch((e) => {
        console.log(url + " <<<<<< Action  >>>>>>>>> Error ", e);

        dispatch({
          type: actions.fail,
          message: e,
        });
      });
  };
};


export const ClearReducerAction = ( params, actions) => {
  return (dispatch) => {
  
        dispatch({
          type: actions.success,
          payload: params,
        });
      }
};