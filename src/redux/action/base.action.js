
// import auth from "@react-native-firebase/auth";
// import OtpVerification from "../../screen/Member/SignIn/OtpVerification";
import * as ActionTypes from "./ActionsType";
import {
  GetAllMovies
} from "../service/base.service";
import { CommonAction } from "./helper/CommonAction";



//LOGIN
export const getMovies = (url, params) => {
  const actions = {
    pending: ActionTypes.GET_MOVIES_PENDING,
    success: ActionTypes.GET_MOVIES_SUCCESS,
    fail: ActionTypes.GET_MOVIES_FAIL,
  };
  console.log("getMovies <<<<<< Action  >>>>>>>>>", url, params);

  return CommonAction(url, params, actions, GetAllMovies);

};


