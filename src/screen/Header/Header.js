import React,{Component} from "react"
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Dimensions,
    TextInput
} from "react-native"

import SearchIcon from "react-native-vector-icons/FontAwesome"
import { RadioButton } from 'react-native-paper';


const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

class Header extends Component {
    constructor(props){
        super(props);
        this.state={
            search:""
        }
    }
    render(){
        return(
            <View >
               
                <TextInput 
                 value={this.state.search}
                 style={{backgroundColor:"#7d7d7d",borderRadius:10}} 
                 placeholder="Search"
                 onChangeText={(value)=>this.setState({
                     search:value
                 })}
                />
                <TouchableOpacity 
                // onPress={()=>}
                style={{position:"absolute",marginLeft:windowWidth/1.2,paddingTop:10}}>
                    <SearchIcon
                     name="search"
                     size={25}
                     color="#bf0000"
                    />
                </TouchableOpacity>
                <View
                style={{
                    borderBottomColor: '#232324',
                    borderBottomWidth: 1,
                    // marginTop:windowHeight/15
                }}
                />
            </View>
        );
    }
}

export default Header