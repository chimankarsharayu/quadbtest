import React,{Component} from "react"
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    Dimensions,
    TextInput,
    ScrollView,
    FlatList
} from "react-native"
import styles from "./styles"

import { connect } from "react-redux";
import { ActionCreators } from "../../redux/action/index";
import { bindActionCreators } from "redux";
import SearchIcon from "react-native-vector-icons/FontAwesome"
import {ALL_MOVIES_URL} from "../../utilities/Filehelper"
import Header from "../Header/Header";
import StarIcon from "react-native-vector-icons/AntDesign"
import axios from "axios";


const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;




class SearchScreen extends Component {

    constructor(props){
        super(props);
        this.state={
            searchKey:""
        }
    }


    async componentDidMount(){
        // await console.log("MOVIES DATA",this.props.route.params.item)
    }

    async getMoviesDetails() {
     await this.props.getMovies();
    }


    async searchMovies(){
        await axios.get("https://api.tvmaze.com/search/shows?q="+this.state.searchKey).then(res => {
            if (res.status == 200) {
                console.log("Movies Data")
                this.props.navigation.navigate("SearchScreen",{
                    item:res.data
                })

            }
        }).catch(e => console.log("ERROR",e))
    }


    renderMovies = ({item})=>{

        return(
            <View style={{padding:7,}}>

                <TouchableOpacity style={styles.movieCard}
                   
                    onPress={()=>{
                   //  alert(item.va)
                   // i18next.changeLanguage(item.value)
                   
                   this.props.navigation.navigate("DetailScreen",
                    {
                      
                      item: item,
                     }
                    )
                   }}
                    >
                        <Image 
                         style={{
                             height:windowHeight/5.8,
                             width:windowWidth/3.8,
                             
                             }}
                         source={{uri:item.show.image!=null?item.show.image.medium:"https://upload.wikimedia.org/wikipedia/commons/1/14/No_Image_Available.jpg?20200913095930"}}
                        />
                        <View style={{paddingLeft:20}}>
                            <Text style={{fontSize:19,fontWeight:"bold",color:"#000"}}>{item.show.name}</Text>
                            <Text style={{color:"#000"}}>Language : {item.show.language}</Text>
                            <Text style={{color:"#000"}}>genres : {item.show.genres[0]}</Text>
                            <View style={{flexDirection:"row"}}>
                                <StarIcon
                                    name="star"
                                    color="#fff269"
                                    size={18}
                                    style={{alignSelf:"center"}}
                                />
                                <Text style={{fontSize:15,fontWeight:"bold",paddingLeft:7}}>{item.show.rating.average}</Text>
                            </View>
                        </View>
                     {/* <Text style={{fontWeight:"bold", fontSize:45,alignSelf:"center",justifyContent:"center",alignItems:"center",alignContent:"center"}}>{item.alphabet}</Text> */}
                </TouchableOpacity>
                
            </View>
            )
        
    }
 

    render(){
        return(
           
            <View style={styles.conatiner}>
                 <View >
               
                    <TextInput 
                       value={this.state.search}
                       style={{backgroundColor:"#7d7d7d",borderRadius:10}} 
                       placeholder="Search"
                       onChangeText={(value)=>this.setState({
                           searchKey:value
                       })}
                    />
                    <TouchableOpacity 
                    onPress={()=>this.searchMovies()}
                    style={{position:"absolute",marginLeft:windowWidth/1.2,paddingTop:10}}>
                        <SearchIcon
                            name="search"
                            size={25}
                            color="#bf0000"
                        />
                    </TouchableOpacity>
                    <View
                    style={{
                        borderBottomColor: '#232324',
                        borderBottomWidth: 1,
                        // marginTop:windowHeight/15
                    }}
                    />
                </View>
                <Text style={{fontSize:20,fontWeight:"bold",color:"#fff",padding:10,marginLeft:8}}>Searched Movies</Text>
                <View style={{justifyContent:"center",marginBottom:windowHeight/10}}>
                    
                    <FlatList              
                        data={this.props.route.params.item}
                        numColumns={1}
                        keyExtractor={item => item.score}
                        renderItem={this.renderMovies}  
                    />
               </View>
            </View>
            
        );
    }
}




function mapStateToProps({ movieReducer }) {
    console.log("++++++++++++ movieReducer++++",movieReducer.movies[0].show.summary)
    return {
     movies:movieReducer.movies
    };
  }
  
  function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(SearchScreen);