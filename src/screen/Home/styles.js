import React  from "react";
import {Dimensions,StyleSheet} from "react-native"

const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

export default {
    conatiner :{
        
        
        backgroundColor:"#000",
        height:windowHeight,
        padding:10
    },
    movieCard:{
        width:windowWidth/3.9,
        height:windowHeight/5.4,
        backgroundColor:"#fff",
      
        },
    image:{
        height:windowHeight/5.4,
        width:windowWidth/3.8,
        alignSelf:"center"
    },
    MovieContainer:{
        alignContent:"center",
        justifyContent:"center",
        alignItems:"center"
    },
    textStyle:{
        fontSize:20,
        fontWeight:"bold",
        color:"#fff",
        padding:10,
        marginLeft:15
    },

}