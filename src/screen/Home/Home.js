import React,{Component} from "react"
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    Dimensions,
    TextInput,
    ScrollView,
    FlatList
} from "react-native"
import styles from "./styles"

import { connect } from "react-redux";
import { ActionCreators } from "../../redux/action/index";
import { bindActionCreators } from "redux";
import SearchIcon from "react-native-vector-icons/FontAwesome"
import {ALL_MOVIES_URL} from "../../utilities/Filehelper"
import Header from "../Header/Header";
import axios from "axios";



const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;




class Home extends Component {

    constructor(props){
        super(props);
        this.state={
           searchKey:""
        }
    }


    async componentDidMount(){
        await this.getMoviesDetails()
    }

    async getMoviesDetails() {
     await this.props.getMovies();
    }

    async searchMovies(){
        await axios.get("https://api.tvmaze.com/search/shows?q="+this.state.searchKey).then(res => {
            if (res.status == 200) {
                console.log("Movies Data")
                this.props.navigation.navigate("SearchScreen",{
                    item:res.data
                })

            }
        }).catch(e => console.log("ERROR",e))
    }

    renderMovies = ({item})=>{

        return(
            <View style={{padding:7,}}>

                <TouchableOpacity style={styles.movieCard}
                    onPress={()=>{
                   
                   this.props.navigation.navigate("DetailScreen",
                    {
                      
                      item: item,
                     }
                    )
                   }}
                    >
                        <Image 
                         style={styles.image}
                         source={{uri:item.show.image.medium}}
                        />
                     {/* <Text style={{fontSize:13,fontWeight:"bold"}}>{item.score}</Text> */}
                     {/* <Text style={{fontWeight:"bold", fontSize:45,alignSelf:"center",justifyContent:"center",alignItems:"center",alignContent:"center"}}>{item.alphabet}</Text> */}
                </TouchableOpacity>
                
            </View>
            )
        
    }
 

    render(){
        return(
           
            <View style={styles.conatiner}>
                 <View >
               
                    <TextInput 
                        value={this.state.search}
                        style={{backgroundColor:"#7d7d7d",borderRadius:10}} 
                        placeholder="Search"
                        onChangeText={(value)=>this.setState({
                            searchKey:value
                        })}
                    />
                    <TouchableOpacity 
                    onPress={()=>this.searchMovies()}
                    style={{position:"absolute",marginLeft:windowWidth/1.2,paddingTop:10}}>
                        <SearchIcon
                            name="search"
                            size={25}
                            color="#bf0000"
                        />
                    </TouchableOpacity>
                    <View
                    style={{
                        borderBottomColor: '#232324',
                        borderBottomWidth: 1,
                        // marginTop:windowHeight/15
                    }}
                    />
                </View>
                <Text style={styles.textStyle}>All Movies</Text>
                <View style={styles.MovieContainer}>
                    
                    <FlatList              
                        data={this.props.movies}
                        numColumns={3}
                        keyExtractor={item => item.score}
                        renderItem={this.renderMovies}  
                    />
               </View>
            </View>
            
        );
    }
}




function mapStateToProps({ movieReducer }) {
    console.log("++++++++++++ movieReducer++++",movieReducer.movies[0].show.summary)
    return {
     movies:movieReducer.movies
    };
  }
  
  function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Home);