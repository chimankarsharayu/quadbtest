import React,{Component} from "react"
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Dimensions,
    TextInput,
    ImageBackground,
    StatusBar,
    ScrollView
} from "react-native"
import styles from "./styles";
import Icon from "react-native-vector-icons/Feather"
import StarIcon from "react-native-vector-icons/AntDesign"
import { RadioButton } from 'react-native-paper';
import HTML from 'react-native-render-html';




const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

class DetailScreen extends Component {
    constructor(props){
        super(props);
        this.state={
           
        }
    }

   source = this.props.route.params.item.show.summary
    render(){
        return(
            <>
            {/* <StatusBar translucent backgroundColor="transparent" /> */}

            <View style={styles.conatiner}>

               {/*  */}
               <View style={{position:"absolute",padding:10}} >
                   <TouchableOpacity
                    onPress={()=>this.props.navigation.goBack()}
                   >
                        <Icon
                            name="arrow-left"
                            color="#fff"
                            size={30}     
                        />
                   </TouchableOpacity>
               </View>
               <View style={{justifyContent:"center"}}>
                  
                <ImageBackground
                    style={styles.image}
                    source={{uri:this.props.route.params.item.show.image!=null?this.props.route.params.item.show.image.original:"https://upload.wikimedia.org/wikipedia/commons/1/14/No_Image_Available.jpg?20200913095930"}}
                />
               </View>
               <View
                style={{
                    borderBottomColor: '#232324',
                    borderBottomWidth: 1,
                    marginTop:windowHeight/15
                }}
                />
                <View style={{flexDirection:"row",padding:20}}>
                    <View style={styles.greyCard}>
                        <StarIcon
                            name="star"
                            color="#fff269"
                            size={20}
                            style={{alignSelf:"center"}}     
                        />
                        <Text style={{color:"white",alignSelf:"center",padding:5}}>{this.props.route.params.item.show.rating.average}</Text>
                    </View>
                    <View style={[styles.greyCard,{marginLeft:10}]}>
                       
                        <Text style={{color:"white",alignSelf:"center",padding:5}}>{this.props.route.params.item.show.genres[0]}</Text>
                    </View>
                  
                </View>

                <ScrollView>
                    <Text style={styles.title}>{this.props.route.params.item.show.name}</Text>
                    <View style={styles.htmlContainer}>
                        <HTML
                            source={{ html: this.props.route.params.item.show.summary }}
                            
                            contentWidth={windowWidth}
                            tagsStyles = {{
                                
                                p:{fontSize: 16, color: '#fff'}
                            }}
                            
                        />
                    </View>
                    
                    
                </ScrollView>
            </View>
            </>
        );
    }
}

export default DetailScreen